import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CC } from './courses.component';
import { CourseComponent } from './course/course.component';
import { CS } from './course/course.services';

@NgModule({
  declarations: [
    AppComponent,
  CC, 
  CourseComponent  ], 
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [CS],
  bootstrap: [AppComponent]
})
export class AppModule { }
